package simplon.co.blogbasket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.validation.Valid;
import simplon.co.blogbasket.entity.PlayersEntity;
import simplon.co.blogbasket.repository.PlayersRepository;
@RestController
@RequestMapping("/api/players")

public class PlayersController {
    @Autowired
    private PlayersRepository playersRepo;

    @GetMapping
    public List<PlayersEntity> all() {
        return playersRepo.findAll();
    }

    @GetMapping("/{id}")
    public PlayersEntity one(@PathVariable int id) {
        PlayersEntity play = playersRepo.findById(id);
        if(play == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return play;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public PlayersEntity add( @Valid @RequestBody PlayersEntity players) {
        playersRepo.persist(players);
        return players;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id); 
        playersRepo.delete(id);
    }

    @PutMapping("/{id}")
    public PlayersEntity replace(@PathVariable int id, @Valid @RequestBody PlayersEntity players) {
        one(id); 
        players.setId(id); 
        playersRepo.update(players);
        return players;
    }

}
