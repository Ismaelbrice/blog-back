package simplon.co.blogbasket.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import jakarta.validation.Valid;
import simplon.co.blogbasket.entity.TeamEntity;
import simplon.co.blogbasket.repository.TeamRepository;

@RestController
@RequestMapping("/api/team")
public class TeamController {
    @Autowired
    private TeamRepository teamRepo;

    @GetMapping
    public List<TeamEntity> all() {
        return teamRepo.findAll();
    }

    @GetMapping("/{id}")
    public TeamEntity one(@PathVariable int id) {
        TeamEntity team = teamRepo.findById(id);
        if(team == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
        return team;
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public TeamEntity add( @RequestBody TeamEntity team) {
        teamRepo.persist(team);
        return team;
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@PathVariable int id) {
        one(id); 
        teamRepo.delete(id);
    }

    @PutMapping("/{id}")
    public TeamEntity replace(@PathVariable int id, @Valid @RequestBody TeamEntity team) {
        one(id); 
        team.setId(id);
        teamRepo.update(team);
        return team;
    }
}
