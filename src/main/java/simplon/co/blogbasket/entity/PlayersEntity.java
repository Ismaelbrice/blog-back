package simplon.co.blogbasket.entity;

public class PlayersEntity {
    private Integer id;
    private String name;
    private Integer number;
    private String position;
    private Integer id_team;
    private String image;
    private TeamEntity team;

    public TeamEntity getTeam() {
        return team;
    }
    public void setTeam(TeamEntity team) {
        this.team = team;
    }
    public PlayersEntity() {
    }
    public PlayersEntity(String name, Integer number, String position, Integer id_team, String image) {
        this.name = name;
        this.number = number;
        this.position = position;
        this.id_team = id_team;
        this.image = image;
    }
    public PlayersEntity(Integer id, String name, Integer number, String position, Integer id_team, String image) {
        this.id = id;
        this.name = name;
        this.number = number;
        this.position = position;
        this.id_team = id_team;
        this.image = image;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public Integer getNumber() {
        return number;
    }
    public void setNumber(Integer number) {
        this.number = number;
    }
    public String getPosition() {
        return position;
    }
    public void setPosition(String position) {
        this.position = position;
    }
    public Integer getId_team() {
        return id_team;
    }
    public void setId_team(Integer id_team) {
        this.id_team = id_team;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }

}
