package simplon.co.blogbasket;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BlogbasketApplication {

	public static void main(String[] args) {
		SpringApplication.run(BlogbasketApplication.class, args);
	}

}
