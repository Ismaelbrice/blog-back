package simplon.co.blogbasket.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import simplon.co.blogbasket.entity.PlayersEntity;
@Repository
public class PlayersRepository {
    @Autowired
    private DataSource dataSource;
    @Autowired 
    private TeamRepository teamRepository;

    public List<PlayersEntity> findAll() {
        List<PlayersEntity> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM players");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                list.add(sqlToplayers(result));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }



    public PlayersEntity findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM players WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return sqlToplayers(result);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }

    private PlayersEntity sqlToplayers(ResultSet result) throws SQLException {
        PlayersEntity player = new PlayersEntity(
            result.getInt("id"),
            result.getString("name"),
            result.getInt("number"),
            result.getString("position"),
            result.getInt("id_team"),
            result.getString("image")
    );
    player.setTeam(teamRepository.findById(result.getInt("id_team")));
        return player;
    }

    public boolean persist(PlayersEntity players) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO players (name,number,position,id_team,image) VALUES (?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, players.getName());
            stmt.setInt(2, players.getNumber());
            stmt.setString(3, players.getPosition());
            stmt.setInt(4, players.getId_team());
            stmt.setString(5, players.getImage());
            
            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                players.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }
    public boolean update(PlayersEntity players) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE players SET name=?,number=?,position=?,id_team=?,image=? WHERE id=?");
            stmt.setString(1, players.getName());
            stmt.setInt(2, players.getNumber());
            stmt.setString(3, players.getPosition());
            stmt.setInt(4, players.getId_team());
            stmt.setString(5, players.getImage());
            stmt.setInt(6, players.getId());

            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }
    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM players WHERE id=?");

            
            stmt.setInt(1, id);

            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }

}
