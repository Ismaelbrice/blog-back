package simplon.co.blogbasket.repository;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import simplon.co.blogbasket.entity.TeamEntity;
@Repository
public class TeamRepository {
     @Autowired
    private DataSource dataSource;

    public List<TeamEntity> findAll() {
        List<TeamEntity> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM team");
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                list.add(sqlTopTeam(result));
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return list;
    }


    public TeamEntity findById(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("SELECT * FROM team WHERE id=?");
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();
            if (result.next()) {
                return sqlTopTeam(result);
            }

        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }
        return null;
    }


    private TeamEntity sqlTopTeam(ResultSet result) throws SQLException {
        return new TeamEntity(
                result.getInt("id"),
                result.getString("name"),
                result.getString("description"),
                result.getString("image")
        );
    }


    public boolean persist(TeamEntity team) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("INSERT INTO team (name, description, image) VALUES (?,?,?)", Statement.RETURN_GENERATED_KEYS);
            stmt.setString(1, team.getName());
            stmt.setString(2, team.getDescription());
            stmt.setString(3, team.getImage());
            
            if(stmt.executeUpdate() == 1) {
                ResultSet keys = stmt.getGeneratedKeys();
                keys.next();
                team.setId(keys.getInt(1));
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }
    public boolean update(TeamEntity team) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("UPDATE team SET name=?, description=?, image=? WHERE id=?");
            stmt.setString(1, team.getName());
            stmt.setString(2, team.getDescription());
            stmt.setString(3, team.getImage());
            stmt.setInt(4, team.getId());
      
            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }
    public boolean delete(int id) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement stmt = connection.prepareStatement("DELETE FROM team WHERE id=?");

            
            stmt.setInt(1, id);

            if(stmt.executeUpdate() == 1) {
                return true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw new RuntimeException("Error in repository", e);
        }

        return false;
    }
}
