-- Active: 1709906099311@@127.0.0.1@3306@team

DROP TABLE IF EXISTS user;
DROP TABLE IF EXISTS players;
DROP TABLE IF EXISTS team;

CREATE TABLE team (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    description VARCHAR(255) NOT NULL,
    image VARCHAR(255) NOT NULL
);

CREATE TABLE players (
    id INT PRIMARY KEY AUTO_INCREMENT,
    name VARCHAR(64) NOT NULL,
    number INT,
    position VARCHAR(250) NOT NULL,
    image VARCHAR(250) NOT NULL,
    id_team INT,
    Foreign Key (id_team) REFERENCES team(id)  ON DELETE CASCADE
);

CREATE TABLE user(
    id INT PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255) NOT NULL UNIQUE, -- Pas obligatoire de mettre UNIQUE, pasque le check sera aussi fait par Spring
    password VARCHAR(255) NOT NULL,
    role VARCHAR(64) NOT NULL
);

INSERT INTO team (name , description , image) VALUES
("Knicks","Les Knicks de New York sont une franchise américaine de basket-ball évoluant dans la National Basketball Association, basée dans l’arrondissement de Manhattan à New York.","https://www.nyc.fr/wp-content/uploads/2015/07/New_York_City-scaled.jpg"),
("Heat","Le Heat de Miami est une franchise de la National Basketball Association basée à Miami, en Floride. Elle évolue au sein de la Conférence Est, dans la Division Sud-Est.","https://www.frontierassicurazioni.it/wp-content/uploads/2022/11/cosa-vedere-miami.jpg"),
("Lakers","Les Lakers de Los Angeles sont une franchise de basket-ball évoluant en NBA basée à Los Angeles en Californie. Les Lakers sont l’une des équipes les plus titrées de l’histoire de la NBA, ayant remporté 17 titres, à égalité avec les Celtics de Boston.","https://upload.wikimedia.org/wikipedia/commons/6/69/Los_Angeles_with_Mount_Baldy.jpg"),
("Clippers","Les Clippers de Los Angeles est une franchise de basket-ball de la NBA basée à Los Angeles, dans l'État de Californie. Le club est l'une des deux équipes de NBA basées à Los Angeles","https://images.ctfassets.net/rc3dlxapnu6k/2T9PMhhjBa6IPkTO61svWc/0ac547f830c61c6030ca6d6910c788c9/iStock-1462206892.jpg?w=1996&h=900&fl=progressive&q=50&fm=jpg"),
("Spurs","Les Spurs de San Antonio sont une franchise de basket-ball basée à San Antonio dans l'État du Texas, aux États-Unis.","https://t3.ftcdn.net/jpg/01/64/32/94/360_F_164329431_46dcmdsoR8MDhMDg42KrcMeYMRjwzg2t.jpg"),
("Celtics","Les Celtics de Boston est une franchise de basket-ball de la NBA basée à Boston dans l'État du Massachusetts aux États-Unis. Fondé le 6 juin 1946.","https://www.guidesulysse.com/images/destinations/iStock-1177426277.jpg"),
("Warriors","Les Warriors de Golden State sont une franchise californienne de basket-ball de la NBA basée à San Francisco, dans la région de la baie de San Francisco. Elle évolue dans la Conférence Ouest.","https://content.r9cdn.net/rimg/dimg/69/1b/cca1e76b-city-13852-1633ad11236.jpg?width=1366&height=768&xhint=1966&yhint=1018&crop=true"),
("Suns","Les Suns de Phoenix sont une franchise de basket-ball de la NBA basée à Phoenix, dans l'Arizona. Fondée en 1968, l’actuel propriétaire de cette franchise est Mat Ishbia.","https://www.visitarizona.com/imager/s3_us-west-1_amazonaws_com/aot-2020/images/Hero-Images/Phoenix-cityscape_b2b0b89039603b931027eb2900b66531.jpg"),
("Raptors","Les Raptors de Toronto sont une équipe professionnelle de basket-ball de la National Basketball Association basée à Toronto en Ontario.","https://www.gostudycanada.com/wp-content/uploads/2021/02/HEADER-TORONTO_2.jpg"),
("Nets","Les Nets de Brooklyn sont une franchise de basket-ball de la National Basketball Association. L'équipe est basée dans l'arrondissement de Brooklyn à New York.","https://www.terre.tv/wp-content/uploads/2022/05/brooklyn-new-york.jpg");

INSERT INTO players (name,number,position,image,id_team) VALUES
("Jalen Brunson",11,"Jalen Brunson, né le 31 août 1996 à New Brunswick, est un joueur américain de basket-ball évoluant au poste de meneur. Il est champion universitaire en 2016 et 2018.","https://imgresizer.eurosport.com/unsafe/1200x0/filters:format(jpeg)/origin-imgresizer.eurosport.com/2023/12/16/3845536-78143648-2560-1440.jpg",1),
("Donte DiVincenzo",0,"Donte DiVincenzo, né le 31 janvier 1997 à Newark, est un joueur américain de basket-ball évoluant au poste d'arrière. Il a remporté les championnats nationaux universitaires.","https://www.newsday.com/_next/image?url=https%3A%2F%2Fcdn.newsday.com%2Fimage-service%2Fversion%2Fc%3AMjFhOWRjODYtZTk1Yy00%3AMzU3YzYz%2Fspknicks240228.jpg%3Ff%3DLandscape%2B16%253A9%26w%3D770%26q%3D1&w=1920&q=80",1),
("Julius  Randle",30,"Julius Deion Randle, né le 29 novembre 1994 à Dallas au Texas, est un joueur américain de basket-ball. Il évolue au poste d'ailier fort. Il termine sa saison universitaire avec 24 double-doubles.","https://cdn.nba.com/manage/2024/02/julius-randle-smile.jpg",1),
("Ogugua Anunoby",8,"Ogugua « OG » Anunoby, né le 17 juillet 1997 à Londres en Angleterre, est un joueur britannico-nigérian de basket-ball évoluant au poste d'ailier. Il a aussi joué pour les Hoosiers de l'Indiana en NCAA.","https://www.si.com/.image/c_fit%2Ch_800%2Cw_1200/MjAzMzI4MTAzMzk5ODI3MDEx/og-anunoby-knicks-1.jpg",1),
("Mitchell Robinson",23,"Mitchell Robinson III, né le 1ᵉʳ avril 1998 à Pensacola en Floride, est un joueur américain de basket-ball évoluant au poste de pivot. En High School, il inscrivait 20,9 points, prenait 13,6 rebonds et effectuait 8,1 contres par match. ","https://sportshub.cbsistatic.com/i/r/2023/12/11/fcd899e6-efcd-46ab-b280-4a7d6bd1bc9f/thumbnail/1200x675/0abe8701d1973649ca85fd64ee07b4c5/mitchell-robinson-getty.png",1),
("Jimmy Butler",22,"ailier","https://media.ouest-france.fr/v1/pictures/MjAyMzEwMzIzMjVhOWU3N2E4ZDI2NTBlNTAwZjNkNjgzYzkwNzA?width=1260&height=708&focuspoint=56%2C35&cropresize=1&client_id=bpeditorial&sign=c71018c3cea0ef739b6667c389ee38723e46a73b703a1517d3d39ec8a8053dad",2),
("Kevin Love",42,"ailier fort","https://library.sportingnews.com/styles/twitter_card_120x120/s3/2023-05/Kevin%20Love%20050123.jpeg?itok=yXz3Nyov",2),
("Bam Adebayo",13,"pivot","https://basketballforever.com/wp-content/uploads/2023/01/bam-FINAL.jpg",2),
("Terry Rozier",12,"arrière","https://cdn.nba.com/teams/uploads/sites/1610612748/2024/01/1200_rozier-transaction.jpg",2),
("Duncan Robinson",55,"ailier","https://www.basketusa.com/wp-content/uploads/2020/08/duncanrobinson.jpg",2),
("Lebron James",6,"ailier","https://images2.thanhnien.vn/528068263637045248/2023/12/22/lebron1-17032315384931989776698.jpg",3),
("Anthony Davis",3,"ailier fort","https://i.kinja-img.com/image/upload/c_fill,f_auto,fl_progressive,g_center,h_675,pg_1,q_80,w_1200/14df1872fbeb0ca6cd096997b4d80a30.jpg",3),
("Austin Reaves",15,"arrière","https://www.basketballnetwork.net/.image/ar_1.91%2Cc_fill%2Ccs_srgb%2Cfl_progressive%2Cg_faces:center%2Cq_auto:good%2Cw_1200/MTk2NjgyOTE4MTg4Mjk1OTk4/austin-reaves.jpg",3),
("Rui Hachimura",28,"ailier","https://i0.wp.com/leroster.com/wp-content/uploads/2023/04/rui-hachimura-getty-1.webp",3),
("D'Angelo Russell",1,"meneur","https://res.cloudinary.com/ybmedia/image/upload/c_crop,h_806,w_1434,x_283,y_12/c_fill,f_auto,h_900,q_auto,w_1600/v1/m/9/3/93e9b0f9573d5588f968a09f3cae0b8604fab517/potential-team-fits-dangelo-russell.jpg",3),
("Kawhi Leonard",2,"ailier","https://www.correo.ca/wp-content/uploads/2024/01/kawhi-leonard-la-clippers-nba_4828884.jpg",4),
("Paul George",13,"ailier","https://imgresizer.eurosport.com/unsafe/1200x0/filters:format(jpeg)/origin-imgresizer.eurosport.com/2023/03/22/3667519-74659308-2560-1440.jpg",4),
("James Harden",1,"meneur","https://i.eurosport.com/2023/11/07/3821039-77653715-1600-900.jpg",4),
("Ivica Zubac",40,"pivot","https://i0.wp.com/nba.thedailydunk.co/wp-content/uploads/2022/06/Ivica-zubac.jpg",4),
("Norman Powell",24,"arrière","https://i.ytimg.com/vi/Zfkih0TzTBI/maxresdefault.jpg",4),
("Victor Wemby",1,"pivot","https://static-images.lpnt.fr/cd-cw809/images/2023/12/29/25839490lpw-25839738-mega-une-jpg_10014016.jpg",5),
("Keldon Johnson",3,"ailier","https://www.yardbarker.com/media/b/5/b57936b59780772716a5af69bac68a8008019951/thumb_16x9/pistons-spurs-trade-proposal-sends-keldon-johnson.jpg?v=1",5),
("Devin Vassell",24,"arrière","https://spurswire.usatoday.com/wp-content/uploads/sites/117/2023/08/USATSI_19494086-e1692908389666.jpg?w=1000&h=600&crop=1",5),
("Zach Collins",23,"pivot","https://images2.minutemediacdn.com/image/upload/c_crop,w_6000,h_3375,x_0,y_315/c_fill,w_1440,ar_16:9,f_auto,q_auto,g_auto/images/GettyImages/mmsport/29/01henm9fee1w7m8w5xah.jpg",5),
("Tre Jones",33,"meneur","https://i0.wp.com/media.ghgossip.com/wp-content/uploads/2023/07/22045254/Tre-Jones-Children.jpg",5),
("Jayson Tatum",0,"ailier","https://imgresizer.eurosport.com/unsafe/1200x0/filters:format(jpeg)/origin-imgresizer.eurosport.com/2023/05/14/3705003-75389988-2560-1440.jpg",6),
("Jaylen Brown",7,"arrière","https://nba.thedailydunk.co/wp-content/uploads/2022/05/Jaylen-Brown.jpg",6),
("Al Horford",42,"pivot","https://yebscore-cms.s3.us-east-2.amazonaws.com/2023/03/image01-3.jpeg",6),
("Derrick White",4,"meneur","https://i.ytimg.com/vi/9uaewMHOVCo/maxresdefault.jpg",6),
("Kristaps Porziņģis",8,"pivot","https://gray-wggb-prod.cdn.arcpublishing.com/resizer/v2/URJFKTHTOJIBVMHVECKBTASIKY.jpg?auth=b72df288d996b8528f11abc4a8064f2b63498b8816a1a0d0a147ede630a3167b&width=800&height=450&smart=true",6),
("Stephen Curry",30,"meneur","https://imgresizer.eurosport.com/unsafe/1200x0/filters:format(jpeg)/origin-imgresizer.eurosport.com/2022/12/16/3508953-71526228-2560-1440.jpg",7),
("Klay Thompson",11,"arrière","https://cdn.vox-cdn.com/thumbor/gH5w4uOGZ23fuag9Nh8pbLLr8dU=/0x0:3600x1800/fit-in/1200x600/cdn.vox-cdn.com/uploads/chorus_asset/file/25378320/2147244790.jpg",7),
("Chris Paul",3,"ailier","https://mim.p7s1.io/pis/ld/9218zChLCVyZ-c1vEwXZAanB-DJ78rnq2V2gRWTHzVV6NywNWMSJCEGmZ4YSENP-WRL0b9lo2z78GmCX050-d_wgXRtzEqVvcLFVt34R7bRrhiVgeCTjam1-7MpB0yYiKgbHAeShJmQ/profile:original?w=1200&rect=0%2C98%2C3600%2C2025",7),
("Draymond green",23,"ailier fort","https://imgresizer.eurosport.com/unsafe/1200x0/filters:format(jpeg)/origin-imgresizer.eurosport.com/2023/11/16/3826646-77765848-2560-1440.jpg",7),
("Andrew Wiggins",22,"ailier","https://a57.foxnews.com/static.foxnews.com/foxnews.com/content/uploads/2024/02/1200/675/Andrew-Wiggins-Golden-State.jpg?ve=1&tl=1",7),
("Kevin Durant",35,"ailier","https://basketballforever.com/wp-content/uploads/2023/02/Durant-Message-.jpg",8),
("Devin Booker",1,"arrière","https://imageio.forbes.com/specials-images/imageserve/64429aea25bb1bec67ebd709/Suns-Clippers-Basketball/960x0.jpg?format=jpg&width=960",8),
("Bradley Beal",3,"meneur","https://aroundthegame.com/wp-content/uploads/2023/12/BradleyBeal_PhoenixSuns_BrightSideoftheSun_AtG-edited.jpeg",8),
("Josh Okogi",2,"ailier","https://arizonasports.com/wp-content/uploads/2023/01/ap_d92676f6ff0d4116b549f87285f603d6-e1695926681852.jpg",8),
("Jusuf Nurkić",27,"pivot","https://www.basketeurope.com/content/images/size/w1304/format/webp/wp-content/uploads/2022/07/jusuf-nurkic-e1658172953573.jpg",8),
("R.J Barrett",9,"ailier","https://www.sportsnet.ca/wp-content/uploads/2024/03/RJ-barrett-1040x572.jpg",9),
("Immanuel Quickley",5,"meneur","https://www.sportsnet.ca/wp-content/uploads/2024/01/quickley-1-1040x572.jpg",9),
("Scottie Barnes",4,"ailier fort","https://ftw.usatoday.com/wp-content/uploads/sites/90/2022/04/Screen-Shot-2022-04-23-at-4.36.19-PM.png?w=1000&h=600&crop=1",9),
("Kelly Olynyk",13,"pivot","https://res.cloudinary.com/ybmedia/image/upload/c_crop,h_431,w_769,x_332,y_83/c_fill,f_auto,h_900,q_auto,w_1600/v1/m/2/5/25707fb9e6e04f22f7815e93f30cb9d748e78dc2/kelly-olynyk-raptors-agree-two-year-contract.jpg",9),
("Gary Trent Jr",33,"ailier","https://library.sportingnews.com/styles/crop_style_16_9_mobile_2x/s3/2023-06/GettyImages-1464548014%20(1).jpg",9),
("Ben Simmons",10,"meneur","https://www.nydailynews.com/wp-content/uploads/2023/11/AP23311563299879-3.jpg?w=1024",10),
("Mikal Bridges",1,"ailier","https://img.sportsv.net/img/article/cover/9/102779/fit-p3AY5L7msg-945x495.jpeg",10),
("Nicolas Claxton",33,"pivot","https://cdn.vox-cdn.com/thumbor/Qlw1r5zM5PcNyRQanFviLUSmBK8=/0x0:5324x3549/1200x800/filters:focal(2229x704:3079x1554)/cdn.vox-cdn.com/uploads/chorus_image/image/68790279/1230090465.0.jpg",10),
("Cam Thomas",24,"arrière","https://media.13newsnow.com/assets/WVEC/images/e5e251d5-6f38-4230-a511-f9c3918bb9bd/e5e251d5-6f38-4230-a511-f9c3918bb9bd_1140x641.jpg",10),
("Cameron Johnson",13,"ailier fort","https://www.newsday.com/_next/image?url=https%3A%2F%2Fcdn.newsday.com%2Fimage-service%2Fversion%2Fc%3AMGUxZmUwYjItM2JhMi00%3ANmJjMTQ1%2Fspnets230330.jpg%3Ff%3DLandscape%2B16%253A9%26w%3D770%26q%3D1&w=1920&q=80",10);


INSERT INTO user (email,password,role) VALUES 
('admin.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_ADMIN'),
('test@test.com', '$2y$10$ExbjqPDMeGm1lZ6GiaDgO.YZqvDucH5TxYjrK2jo1JxI3V0RbHwvq', 'ROLE_USER');