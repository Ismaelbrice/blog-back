package simplon.co.blogbasket.controller;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.web.servlet.MockMvc;



@SpringBootTest 
@AutoConfigureMockMvc 
@Sql("/database.sql") 

public class PlayersControllerTest {
    @Autowired
    MockMvc mvc;

    @Test
    void testGetAll() throws Exception {
        mvc.perform(get("/api/players"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$[*]['name']").exists())
        .andExpect(jsonPath("$[0]['position']").value("meneur"));
    }

    @Test
    void testGetOneSuccess() throws Exception {
        mvc.perform(get("/api/players/1"))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").isNumber())
        .andExpect(jsonPath("$['name']").isString())
        .andExpect(jsonPath("$['number']").isNumber())
        .andExpect(jsonPath("$['position']").isString());
    }
    @Test
    void testGetOneNotFound() throws Exception {
        mvc.perform(get("/api/players/1000"))
        .andExpect(status().isNotFound());
    }
    @Test
    void testPostPlayers() throws Exception {
        mvc.perform(
            post("/api/players")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "name": "Jalen Brunson",
                    "number":"11",
                    "position":"meneur"
                }
            """)
            ).andExpect(status().isCreated())
            .andExpect(jsonPath("$['id']").isNumber());
    }
    @Test
    void testPostInvalidPlayers() throws Exception {
        mvc.perform(
            post("/api/players")
            .contentType(MediaType.APPLICATION_JSON)
            .content("""
                {
                    "name":"Jalen Brunson",
                    "position":"meneur"
                }
            """)
            ).andExpect(status().isBadRequest());
    }

    @Test
    void testDeletePlayers() throws Exception {
        mvc.perform(delete("/api/players/1"))
        .andExpect(status().isNoContent());
    }

    @Test
    void testPutPlayers() throws Exception {
        mvc.perform(put("/api/players/1")
        .contentType(MediaType.APPLICATION_JSON)
        .content("""
            {
                "id": 1,
                "name": "Updated Players",
                "number":"15"
                "position":"meneur",
            }
        """)).andExpect(status().isOk())
        .andExpect(jsonPath("$['id']").value(1))
        .andExpect(jsonPath("$['name']").value("Updated players"))
        .andExpect(jsonPath("$['number']").value("15"))
        .andExpect(jsonPath("$['position']").value("meneur"));
    }

}
